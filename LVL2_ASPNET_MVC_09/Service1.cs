﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.IO;

namespace LVL2_ASPNET_MVC_09
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed);
                tmrExecutor.Interval = 60000;
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {

                throw;
            }

        }
        private void tmrExecutor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string path = "C:\\Sample.txt";
            using(StreamWriter writer = new StreamWriter(path,true))
            {
                writer.WriteLine(String.Format("Windows Service is clled on " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")+ ""));
                    writer.Close();
            }
            Thread.Sleep(ScheduleTime * 60 * 1000);
        }
        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
